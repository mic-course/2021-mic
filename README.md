# 2021 MIC Course Content
## Schedule
- [Week 1 Schedule](admin/week1_schedule.md)
- [Week 2 Schedule](admin/week2_schedule.md)
- [Office Hours](admin/office_hours.md)
- [Managing Notes](admin/managing_file_modifications.Rmd)
- [After the MIC Course](admin/post_course_resources.Rmd)
